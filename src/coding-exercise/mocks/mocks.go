package mocks

import (
  "../models"
)

// Inventory Dao Mock object
type MockInventoryDaoImpl struct {
  Item *models.InventoryItem
  Err error
}

func (dao MockInventoryDaoImpl) GetInventoryItem(
      itemId string) (*models.InventoryItem, error) {
  return dao.Item, dao.Err
}

// Suggestion Service Mock object
type MockSuggestionServiceImpl struct {
  Suggestions []models.InventoryItem
  Err error
}

func (service MockSuggestionServiceImpl) GetSuggestions(
    itemId string, maxNumberOfSuggestions int) ([]models.InventoryItem, error) {
  return service.Suggestions, service.Err
}
