package utils

import(
    "math/rand"
    "time"
)

// Util func to return a random number between min and max
func Random(min, max int) int {
    rand.Seed(time.Now().Unix())
    return rand.Intn(max - min) + min
}
