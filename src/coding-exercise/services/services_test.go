package services

import (
  "testing"
  "fmt"
  "strconv"
  "errors"
  "../models"
  "../mocks"
  "../utils"
  "github.com/stretchr/testify/assert"
)

//Unit tests go here
//You may choose any go mocking framework you think works best for you
//Please aim to cover all the scenarios/paths in the GetItemOrSuggestions method

// Test items are by default created with this number of items in stock
const DEFAULT_NR_IN_STOCK = 25

// Helper function to create slice of suggestions for testing
func MakeSuggestionsSlice(n int) ([]models.InventoryItem) {
  testSuggestions := []models.InventoryItem{}
  for i := 0; i < n; i++ {
    name := "Item" + strconv.Itoa(i)
    item := models.InventoryItem{name, DEFAULT_NR_IN_STOCK}
    testSuggestions = append(testSuggestions, item)
  }
  return testSuggestions
}

// Tests for GetItemOrSuggestions

// Inventory Dao returns an error
func TestGetItemOrSuggestions_ErrorDao(t *testing.T) {
  err := errors.New("Error in DAO")
  testItem := models.InventoryItem{"Item1", 50}
  dao := mocks.MockInventoryDaoImpl{&testItem, err}
  service := mocks.MockSuggestionServiceImpl{nil, nil}
  inventoryService := NewInventoryService(dao, service)
  _, _, error := inventoryService.GetItemOrSuggestions("Item1", 2)
  assert.Equal(t, "Error in DAO", error.Error(), "Expected Error in DAO")
}

// Suggestion service returns an error
func TestGetItemOrSuggestions_ErrorSuggestionService(t *testing.T) {
  err := errors.New("Error in Suggestion Service")
  testItem := models.InventoryItem{"Item1", 0}
  dao := mocks.MockInventoryDaoImpl{&testItem, nil}
  service := mocks.MockSuggestionServiceImpl{nil, err}
  inventoryService := NewInventoryService(dao, service)
  _, _, error := inventoryService.GetItemOrSuggestions("Item1", 2)
  assert.Equal(t, "Error in Suggestion Service", error.Error(),
    "Expected Error inn Suggestion Service")
}

// Inventory Dao cannot find item in the inventory, returns nil item
func TestGetItemOrSuggestions_NoSuchItem(t *testing.T) {
  dao := mocks.MockInventoryDaoImpl{nil, nil}
  service := mocks.MockSuggestionServiceImpl{nil, nil}
  inventoryService := NewInventoryService(dao, service)
  _, _, error := inventoryService.GetItemOrSuggestions("Item1", 2)
  assert.NotNil(t, error, "Expected error")
  assert.Equal(t, "Item does not exist", error.Error(), "Expected no item")
}

// Inventory Dao returns the item
func TestGetItemOrSuggestions_ItemInStock(t *testing.T) {
  testItem := models.InventoryItem{"Item1", 50}
  dao := mocks.MockInventoryDaoImpl{&testItem, nil}
  service := mocks.MockSuggestionServiceImpl{nil, nil}
  inventoryService := NewInventoryService(dao, service)
  item, _, error := inventoryService.GetItemOrSuggestions("Item1", 2)
  assert.Nil(t, error, "Expected no error")
  assert.Equal(t, testItem, *item, "Expected item Item1")
}

// Inventory Dao returns an item but not the requested one
// func TestGetItemOrSuggestions_WrongItem(t *testing.T) {
//   testItem := models.InventoryItem{"Item1", 50}
//   dao := mocks.MockInventoryDaoImpl{&testItem, nil}
//   service := mocks.MockSuggestionServiceImpl{nil, nil}
//   inventoryService := NewInventoryService(dao, service)
//   _, _, error := inventoryService.GetItemOrSuggestions("Item2", 2)
//   assert.NotNil(t, error,
//     "Expected error. Item1 in inventory but asked for item2!")
// }

// Inventory Dao returns an item but no more left in stock.
// Suggestion service returns an empty slice
func TestGetItemOrSuggestions_EmptySuggestions(t *testing.T) {
  testItem := models.InventoryItem{"Item1", 0}
  dao := mocks.MockInventoryDaoImpl{&testItem, nil}
  service := mocks.MockSuggestionServiceImpl{[]models.InventoryItem{}, nil}
  inventoryService := NewInventoryService(dao, service)
  item, suggestions, error :=
      inventoryService.GetItemOrSuggestions("Item1", 2)
  assert := assert.New(t)
  assert.Nil(error, "Expected no error")
  assert.Equal(testItem, *item, "Expected item Item1")
  assert.Equal(0, len(suggestions), "Expected no suggestions")
}

// Inventory Dao returns an item but no more left in stock.
// Suggestion service returns two suggestions, five were requested
func TestGetItemOrSuggestions_TwoSuggestionsFiveAsekd(t *testing.T) {
  testItem := models.InventoryItem{"TestItem", 0}
  dao := mocks.MockInventoryDaoImpl{&testItem, nil}
  testSuggestions := MakeSuggestionsSlice(2)
  service := mocks.MockSuggestionServiceImpl{testSuggestions, nil}
  inventoryService := NewInventoryService(dao, service)
  item, suggestions, error :=
      inventoryService.GetItemOrSuggestions("TestItem", 5)
  assert := assert.New(t)
  assert.Nil(error, "Expected no error")
  assert.Equal(testItem, *item, "Expected item TestItem")
  assert.Equal(2, len(suggestions), "Expected 2 suggestions")
  for i := 0; i < 2; i++ {
    name := "Item" + strconv.Itoa(i)
    item := models.InventoryItem{name, DEFAULT_NR_IN_STOCK}
    assert.Equal(item, suggestions[i], "Expected Item.")
  }
}

// Inventory Dao returns an item but no more left in stock.
// Suggestion service returns five suggestions, five were requested
func TestGetItemOrSuggestions_FiveSuggestionsFiveAsked(t *testing.T) {
  testItem := models.InventoryItem{"TestItem", 0}
  dao := mocks.MockInventoryDaoImpl{&testItem, nil}
  testSuggestions := MakeSuggestionsSlice(5)
  service := mocks.MockSuggestionServiceImpl{testSuggestions, nil}
  inventoryService := NewInventoryService(dao, service)
  item, suggestions, error :=
      inventoryService.GetItemOrSuggestions("TestItem", 5)
  assert := assert.New(t)
  assert.Nil(error, "Expected no error")
  assert.Equal(testItem, *item, "Expected item TestItem")
  assert.Equal(5, len(suggestions), "Expected 5 suggestions")
  for i := 0; i < 5; i++ {
    name := "Item" + strconv.Itoa(i)
    item := models.InventoryItem{name, DEFAULT_NR_IN_STOCK}
    assert.Equal(item, suggestions[i], "Expected Item.")
  }
}

// Inventory Dao returns an item but no more left in stock.
// Suggestion service returns two suggestions, fifteen (i.e. more than
// the default value define in code) were requested
func TestGetItemOrSuggestions_TwoSuggestionsFifteenAsked(t *testing.T) {
  testItem := models.InventoryItem{"Item1", 0}
  dao := mocks.MockInventoryDaoImpl{&testItem, nil}
  testSuggestions := MakeSuggestionsSlice(2)
  service := mocks.MockSuggestionServiceImpl{testSuggestions, nil}
  inventoryService := NewInventoryService(dao, service)
  item, suggestions, error :=
      inventoryService.GetItemOrSuggestions("Item1", 15)
  assert := assert.New(t)
  assert.Nil(error, "Expected no error")
  assert.Equal(testItem, *item, "Expected item Item1")
  assert.Equal(2, len(suggestions), "Expected 2 suggestions")
  for i := 0; i < 2; i++ {
    name := "Item" + strconv.Itoa(i)
    item := models.InventoryItem{name, DEFAULT_NR_IN_STOCK}
    assert.Equal(item, suggestions[i], "Expected Item.")
  }
}

// Inventory Dao returns an item but no more left in stock.
// Suggestion service returns two suggestions, only one was requested
// func TestGetItemOrSuggestions_TwoSuggestionsOneAsked(t *testing.T) {
//   testItem := models.InventoryItem{"Item1", 0}
//   dao := mocks.MockInventoryDaoImpl{&testItem, nil}
//   testSuggestions := MakeSuggestionsSlice(2)
//   service := mocks.MockSuggestionServiceImpl{testSuggestions, nil}
//   inventoryService := NewInventoryService(dao, service)
//   _, _, error :=
//       inventoryService.GetItemOrSuggestions("Item1", 1)
//   assert := assert.New(t)
//   assert.NotNil(error,
//     "Expected error. Suggestion service returned more items than requested!")
// }

// Inventory Dao returns an item but no more left in stock.
// Suggestion service returns a random number of suggestions
func TestGetItemOrSuggestions_Random(t *testing.T) {
  r := utils.Random(1, 30)
  testItem := models.InventoryItem{"TestItem", 0}
  dao := mocks.MockInventoryDaoImpl{&testItem, nil}
  testSuggestions := MakeSuggestionsSlice(r)
  service := mocks.MockSuggestionServiceImpl{testSuggestions, nil}
  inventoryService := NewInventoryService(dao, service)
  item, suggestions, error :=
      inventoryService.GetItemOrSuggestions("TestItem", r)
  assert := assert.New(t)
  assert.Nil(error, "Expected no error")
  assert.Equal(testItem, *item, "Expected item TestItem")
  assert.Equal(r, len(suggestions), fmt.Sprintf("Expected %d suggestions", r))
  for i := 0; i < r; i++ {
    name := "Item" + strconv.Itoa(i)
    item := models.InventoryItem{name, DEFAULT_NR_IN_STOCK}
    assert.Equal(item, suggestions[i], "Expected Item.")
  }
}
