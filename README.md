## Overview

The goal of this coding exercise is to write the unit tests for the one method in the InventoryServiceImpl. The service class follows a dependency injection pattern with dependencies passed in on creation of the implementation struct.

#### Mocking Libraries

Feel free to use any mocking framework that you feel works best.

#### Coverage/Scenarios

The main goal is to cover all the different scenarios that could occur in the service method and assert the expecations.
